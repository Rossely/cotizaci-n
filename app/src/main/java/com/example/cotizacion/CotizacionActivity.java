package com.example.cotizacion;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class CotizacionActivity extends AppCompatActivity {
    private TextView txtFolio;
    private TextView txtcliente;
    private TextView txtDescripcion;
    private TextView txtValordelAuto;
    private TextView txtPagoMensual;
    private TextView txtPagoInicial;

    private RadioButton rdb12;
    private RadioButton rdb24;
    private RadioButton rdb36;
    private RadioButton rdb48;

    private Button btnRegresar;
    private Button btnLimpiar;
    private Button btnCalcular;

    private Cotizacion cotiza;
    private TextView lblPagoMensual;
    private TextView lblPagoInicial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cotizacion);
        final TextView txtFolio = (TextView) findViewById(R.id.txtFolio);
        final TextView txtcliente = (TextView) findViewById(R.id.txtcliente);
        final TextView txtDescripcion = (TextView) findViewById(R.id.txtDescripcion);
        final TextView txtValorAuto = (TextView) findViewById(R.id.txtValordelAuto);
        final TextView txtPagoInicial = (TextView) findViewById(R.id.txtPagoInicial);
        final TextView lblPagoMensual = (TextView) findViewById(R.id.lblPagoMensual);
        final TextView lblPagoInicial = (TextView) findViewById(R.id.lblPagoInicial);
        final RadioButton rdb12 = (RadioButton) findViewById(R.id.rdb12);
        final RadioButton rdb24 = (RadioButton) findViewById(R.id.rdb24);
        final RadioButton rdb36 = (RadioButton) findViewById(R.id.rdb36);
        final RadioButton rdb48 = (RadioButton) findViewById(R.id.rdb48);
        Button btnRegresar = (Button) findViewById(R.id.btnRegresar);
        Button btnLimpiar= (Button) findViewById(R.id.btnLimpiar);
        Button btnCalcular = (Button) findViewById(R.id.btnCalcular);

        cotiza = new Cotizacion();
        txtFolio.setText("Folio: " + String.valueOf( cotiza.generaFolio()));

        Bundle datos = getIntent().getExtras();
        String cliente = datos.getString("Cliente");
        txtcliente.setText("Cliente: " + cliente);
        rdb12.setSelected(true);
        rdb24.setSelected(false);
        rdb36.setSelected(false);
        rdb48.setSelected(false);





        btnCalcular.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                if (txtDescripcion.getText().toString().matches("") ||
                        txtValorAuto.getText().toString().matches("") ||
                        txtPagoInicial.getText().toString().matches("")){
                    Toast.makeText(CotizacionActivity.this, "Faltó Capturar Información perro",
                            Toast.LENGTH_SHORT).show();
                    txtDescripcion.requestFocus();
                }
                else{
                    int plazos = 0;
                    float enganche = 0;
                    float pagomensual = 0.0f;

                    if (rdb12.isChecked())plazos = 12;
                    if (rdb24.isChecked())plazos = 24;
                    if (rdb36.isChecked()) plazos = 36;
                    if (rdb48.isChecked())plazos = 48;



                    cotiza.setDescripcion(txtDescripcion.getText().toString());
                    cotiza.setValorAuto(Float.parseFloat(txtValorAuto.getText().toString()));
                    cotiza.setPorEnganche(Float.parseFloat(txtPagoInicial.getText().toString()));
                    cotiza.setPlazos(plazos);

                    enganche = cotiza.calcularPagoInicial();
                    pagomensual = cotiza.calcularPagoMensual();

                    lblPagoInicial.setText("Pago Inicial: $" + String.valueOf(enganche));
                    lblPagoMensual.setText("Pago Mensual: $" + String.valueOf(pagomensual));




                }
            }
        });


        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                txtDescripcion.setText("");
                txtPagoInicial.setText("");
                txtValorAuto.setText("");
                lblPagoInicial.setText("Pago Inicial:");
                lblPagoMensual.setText("Pago Mensual:");
                rdb12.setSelected(true);
                rdb24.setSelected(false);
                rdb36.setSelected(false);
                rdb48.setSelected(false);
            }
        });
    }


    }



